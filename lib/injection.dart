import 'package:albumsapp/core/data/base_local_data_source.dart';
import 'package:albumsapp/core/data/base_remote_datasource.dart';
import 'package:albumsapp/core/data/base_repository.dart';
import 'package:albumsapp/core/network/network_info.dart';
import 'package:albumsapp/core/util/constants.dart';
import 'package:albumsapp/core/util/input_validator.dart';
import 'package:albumsapp/feature/albums/data/data_source/albums_remote_data_source.dart';
import 'package:albumsapp/feature/albums/data/repository/albums_repository_impl.dart';
import 'package:albumsapp/feature/albums/domain/repository/albums_repository.dart';
import 'package:albumsapp/feature/albums/domain/usecase/get_albums.dart';
import 'package:albumsapp/feature/albums/presentation/bloc/albums_bloc.dart';
import 'package:albumsapp/feature/login/data/datasource/login_local_data_source.dart';
import 'package:albumsapp/feature/login/data/repository/login_repository_impl.dart';
import 'package:albumsapp/feature/login/domain/repository/login_repository.dart';
import 'package:albumsapp/feature/login/domain/usecase/login.dart';
import 'package:albumsapp/feature/login/presentation/bloc/login_bloc.dart';
import 'package:albumsapp/feature/photos/data/data_source/photos_remote_data_source.dart';
import 'package:albumsapp/feature/photos/data/repository/photos_repository_impl.dart';
import 'package:albumsapp/feature/photos/domain/repository/photos_repository.dart';
import 'package:albumsapp/feature/photos/domain/usecase/get_photos.dart';
import 'package:albumsapp/feature/photos/presentation/bloc/photos_bloc.dart';
import 'package:albumsapp/feature/splash/data/datasource/splash_local_data_source.dart';
import 'package:albumsapp/feature/splash/data/repository/splash_repository_impl.dart';
import 'package:albumsapp/feature/splash/domain/repository/splah_repository.dart';
import 'package:albumsapp/feature/splash/domain/usecase/is_logged_in.dart';
import 'package:albumsapp/feature/splash/presentation/bloc/splash_bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'feature/login/data/datasource/login_remote_data_source.dart';

final sl = GetIt.instance;

///
/// This function is responsible for creating the dependency injection graph
///
Future<void> init() async {
  //! External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => Connectivity());
  sl.registerLazySingleton(() => InputValidator());
  sl.registerLazySingleton(
    () {
      final dio = Dio(
        BaseOptions(
          connectTimeout: 20000,
          baseUrl: Endpoints.BASE_URL,
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
          responseType: ResponseType.plain,
        ),
      );
      dio.interceptors.add(
        LogInterceptor(
          responseBody: true,
          requestBody: true,
          responseHeader: true,
          requestHeader: true,
          request: true,
        ),
      );
      return dio;
    },
    instanceName: 'backend',
  );

  //! Core
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  // Data sources
  sl.registerLazySingleton<BaseRemoteDataSource>(
    () => BaseRemoteDataSourceImpl(dio: sl('backend')),
  );

  sl.registerLazySingleton<BaseLocalDataSource>(
    () => BaseLocalDataSourceImpl(sharedPreferences: sl()),
  );

  sl.registerLazySingleton<SplashLocalDataSource>(
    () => SplashLocalDataSourceImpl(
      sharedPreferences: sl(),
    ),
  );

  sl.registerLazySingleton<LoginRemoteDataSource>(
    () => LoginRemoteDataSourceImpl(dio: sl('backend')),
  );
  sl.registerLazySingleton<LoginLocalDataSource>(
    () => LoginLocalDataSourceImpl(sharedPreferences: sl()),
  );

  sl.registerLazySingleton<AlbumsRemoteDataSource>(
    () => AlbumsRemoteDataSourceImpl(dio: sl('backend')),
  );
  sl.registerLazySingleton<PhotosRemoteDataSource>(
    () => PhotosRemoteDataSourceImpl(dio: sl('backend')),
  );

  // Repository

  sl.registerLazySingleton<BaseRepository>(
    () => BaseRepositoryImpl(
      networkInfo: sl(),
      baseLocalDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<LoginRepository>(
    () => LoginRepositoryImpl(
      networkInfo: sl(),
      loginLocalDataSource: sl(),
      loginRemoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<PhotosRepository>(
    () => PhotosRepositoryImpl(
      networkInfo: sl(),
      photosRemoteDataSource: sl(),
      baseLocalDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<AlbumsRepository>(
    () => AlbumsRepositoryImpl(
      networkInfo: sl(),
      baseLocalDataSource: sl(),
      albumsRemoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<SplashRepository>(
    () => SplashRepositoryImpl(
      networkInfo: sl(),
      splashLocalDataSource: sl(),
    ),
  );

  // Use cases
  sl.registerLazySingleton(() => IsLoggedIn(repository: sl()));
  sl.registerLazySingleton(() => Login(repository: sl()));
  sl.registerLazySingleton(() => GetAlbums(repository: sl()));
  sl.registerLazySingleton(() => GetPhotos(repository: sl()));

  //! Features
  // Bloc
  sl.registerFactory(
    () => SplashBloc(
      isLoggedIn: sl(),
    ),
  );
  sl.registerFactory(
    () => PhotosBloc(
      getPhotos: sl(),
    ),
  );

  sl.registerFactory(
    () => LoginBloc(
      login: sl(),
      inputValidator: sl(),
    ),
  );

  sl.registerFactory(
    () => AlbumsBloc(
      getAlbums: sl(),
    ),
  );
}
