import 'package:albumsapp/core/data/base_local_data_source.dart';
import 'package:albumsapp/core/error/exceptions.dart';
import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/core/network/network_info.dart';
import 'package:albumsapp/core/util/constants.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

typedef FutureEitherOr<T> = Future<Either<Failure, T>> Function();
typedef FutureEitherOrWithUserID<T> = Future<Either<Failure, T>> Function(
    int userID);

abstract class BaseRepository {
  Future<Either<Failure, T>> checkNetwork<T>(FutureEitherOr<T> body);

  Future<Either<Failure, int>> getUserID();

  Future<Either<Failure, T>> requestWithUserID<T>(
      FutureEitherOrWithUserID<T> body);

  Future<Either<Failure, T>> requestAPI<T>(FutureEitherOr<T> body);
}

class BaseRepositoryImpl implements BaseRepository {
  final BaseLocalDataSource baseLocalDataSource;
  final NetworkInfo networkInfo;

  BaseRepositoryImpl(
      {@required this.baseLocalDataSource, @required this.networkInfo});

  @override
  Future<Either<Failure, T>> checkNetwork<T>(FutureEitherOr<T> body) async {
    if (await networkInfo.isConnected) {
      return body();
    } else {
      return Left(ServerFailure(ErrorCode.NO_INTERNET_CONNECTION));
    }
  }

  @override
  Future<Either<Failure, T>> requestAPI<T>(
    FutureEitherOr<T> body,
  ) async {
    print('requestWithToken');
    return await checkNetwork<T>(() async {
      try {
        return body();
      } catch (e) {
        print('e is $e');
        return Left(ServerFailure(ErrorCode.NO_INTERNET_CONNECTION));
      }
    });
  }

  @override
  Future<Either<Failure, int>> getUserID() async {
    final userID = await baseLocalDataSource.userID;
    print(userID);
    if (userID != null) {
      return Right(userID);
    } else {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, T>> requestWithUserID<T>(
      FutureEitherOrWithUserID<T> body) async {
    return await checkNetwork<T>(() async {
      try {
        final userID = await getUserID();
        return await userID.fold(
          (failure) => Left(CacheFailure()),
          (id) async {
            return body(id);
          },
        );
      } catch (e) {
        print('e is $e');
        if (e is ServerException)
          return Left(ServerFailure(e.errorCode));
        else
          return Left(ServerFailure(ErrorCode.SERVER_ERROR));
      }
    });
  }
}
