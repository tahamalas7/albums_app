class SharedPreferencesKeys {
  static const ID = 'id';
  static const IS_LOGGED_IN = 'is_logged_in';
  static const USERNAME = 'username';
}
