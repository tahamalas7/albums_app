import 'dart:convert';

import 'package:albumsapp/core/error/exceptions.dart';
import 'package:albumsapp/core/util/constants.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

abstract class BaseRemoteDataSource {
  @protected
  Future<dynamic> performGetRequest(
    String endpoint,
  );
}

class BaseRemoteDataSourceImpl extends BaseRemoteDataSource {
  final Dio dio;

  BaseRemoteDataSourceImpl({@required this.dio});

  @override
  Future<dynamic> performGetRequest(String endpoint) async {
    print('performGetRequest');
    try {
      final response = await dio.get(
        endpoint,
      );
      if (response.statusCode == 200) {
        return json.decode(response.data);
      } else if (response.statusCode == 401) {
        throw ServerException(ErrorCode.WRONG_INPUT);
      } else if (response.statusCode == 403) {
        throw ServerException(ErrorCode.FORBIDDEN);
      } else
        throw ServerException(ErrorCode.SERVER_ERROR);
    } catch (e) {
      if (e is ServerException)
        throw e;
      else
        throw ServerException(ErrorCode.SERVER_ERROR);
    }
  }
}
