import 'package:albumsapp/core/data/shared_preferences_keys.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class BaseLocalDataSource {
  Future<int> get userID;
}

class BaseLocalDataSourceImpl implements BaseLocalDataSource {
  final SharedPreferences sharedPreferences;

  BaseLocalDataSourceImpl({@required this.sharedPreferences});

  @override
  Future<int> get userID =>
      Future.value(sharedPreferences.getInt(SharedPreferencesKeys.ID));
}
