import 'package:dio/dio.dart';

///
/// The [Endpoints] class is a way to separate the API's endpoints and base url from the
///
class Endpoints {
  static const BASE_URL = 'https://jsonplaceholder.typicode.com//';

  static const LOGIN = 'users';

  static getAlbums(int userID) => 'users/$userID/albums';

  static getPhotos(int albumID) => 'albums/$albumID/photos';
}


///
/// The [ErrorCode] enum is responsible of stating all the possible issues that can be retrieved from the API
///
enum ErrorCode {
  SERVER_ERROR,
  NO_INTERNET_CONNECTION,
  WRONG_INPUT,
  FORBIDDEN,

}
