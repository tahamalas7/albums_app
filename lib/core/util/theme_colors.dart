import 'package:flutter/material.dart';

class ThemeColors {
  static const Color textColor = Color(0xFF707070);
  static const Color backgroundColor = Color(0xFFEAF3FA);
  static const Color accentColor = Color.fromRGBO(255, 192, 159, 1);
}
