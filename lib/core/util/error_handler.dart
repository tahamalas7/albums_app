import 'package:albumsapp/core/util/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ErrorHandler {
  final BuildContext context;

  ErrorHandler({
    @required this.context,
  });

  void showError(ErrorCode errorCode) {
    switch (errorCode) {
      case ErrorCode.SERVER_ERROR:
        Fluttertoast.showToast(
          msg: 'There was an error from our end, please try again later!',
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
        );
        break;
      case ErrorCode.NO_INTERNET_CONNECTION:
        Fluttertoast.showToast(
          msg: "You don't have an internet conncetion, please try again later",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
        );
        break;
      case ErrorCode.WRONG_INPUT:
        Fluttertoast.showToast(
          msg: "Make sure you entered the right input",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
        );
        break;
      case ErrorCode.FORBIDDEN:
        Fluttertoast.showToast(
          msg: "Your request was denied",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
        );
        break;
    }
  }
}
