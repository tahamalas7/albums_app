import 'package:albumsapp/core/error/failures.dart';
import 'package:dartz/dartz.dart';

class InputValidator {
  Either<Failure, String> validateNameInput(String str) {
    if (str.length > 2)
      return Right(str);
    else
      return Left(InvalidInputFailure());
  }
}

class InvalidInputFailure extends Failure {}
