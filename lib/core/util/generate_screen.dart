import 'package:albumsapp/feature/albums/presentation/page/albums_page.dart';
import 'package:albumsapp/feature/login/presentation/page/login_page.dart';
import 'package:albumsapp/feature/photos/presentation/page/photos_page.dart';
import 'package:albumsapp/feature/splash/presentation/page/splash_page.dart';
import 'package:flutter/material.dart';

///
/// This class contains all the
///
class GenerateScreen {
  static Route<dynamic> onGenerate(RouteSettings value) {
    String name = value.name;
    print("the name is $name");
    final args = value.arguments;
    switch (name) {
      case NameScreen.SPLASH_PAGE:
        {
          return MaterialPageRoute(builder: (context) => SplashPage());
        }
      case NameScreen.LOGIN_PAGE:
        {
          return MaterialPageRoute(builder: (context) => LoginPage());
        }
      case NameScreen.ALBUMS_PAGE:
        {
          return MaterialPageRoute(builder: (context) => AlbumsPage());
        }
      case NameScreen.PHOTOS_PAGE:
        {
          if (args is Map<String, dynamic>)
            return MaterialPageRoute(
                builder: (context) => PhotosPage(
                      albumID: args['id'],
                      albumTitle: args['title'],
                    ));
          return _errorRoute();
        }
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Error'),
          ),
          body: Center(
            child: Text('ERROR'),
          ),
        );
      },
    );
  }
}

class NameScreen {
  static const String SPLASH_PAGE = "/";
  static const String ALBUMS_PAGE = "/albums";
  static const String LOGIN_PAGE = "/login";
  static const String PHOTOS_PAGE = "/photos";
}
