import 'package:albumsapp/core/util/constants.dart';
import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  Failure([List properties = const <dynamic>[]]) : super(properties);
}

// General failures
class ServerFailure extends Failure {
  final ErrorCode errorCode;

  ServerFailure(this.errorCode);
}

class CacheFailure extends Failure {}
