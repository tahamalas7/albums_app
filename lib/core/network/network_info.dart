import 'dart:async';
import 'package:connectivity/connectivity.dart';

abstract class NetworkInfo {
  ///
  /// This function checks if the device is connected to a WiFi or mobile network or not
  /// Output:
  ///   - Boolean that indicates if the device is connected or not
  ///
  Future<bool> get isConnected;
}

class NetworkInfoImpl implements NetworkInfo {
  final Connectivity connectivity;

  NetworkInfoImpl(this.connectivity);

  @override
  Future<bool> get isConnected async =>
      ((await connectivity.checkConnectivity()) != ConnectivityResult.none);
}
