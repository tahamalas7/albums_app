import 'package:albumsapp/core/data/base_local_data_source.dart';
import 'package:albumsapp/core/data/base_repository.dart';
import 'package:albumsapp/core/error/exceptions.dart';
import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/core/network/network_info.dart';
import 'package:albumsapp/feature/albums/data/data_source/albums_remote_data_source.dart';
import 'package:albumsapp/feature/albums/domain/entity/album_entity.dart';
import 'package:albumsapp/feature/albums/domain/repository/albums_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

class AlbumsRepositoryImpl extends BaseRepositoryImpl
    implements AlbumsRepository {
  final AlbumsRemoteDataSource albumsRemoteDataSource;

  AlbumsRepositoryImpl({
    @required this.albumsRemoteDataSource,
    @required BaseLocalDataSource baseLocalDataSource,
    @required NetworkInfo networkInfo,
  }) : super(
          networkInfo: networkInfo,
          baseLocalDataSource: baseLocalDataSource,
        );

  @override
  Future<Either<Failure, List<AlbumEntity>>> getAlbums() async {
    return await requestWithUserID<List<AlbumEntity>>(
      (int userID) async {
        try {
          final albums = await albumsRemoteDataSource.getAlbums(userID);
          return Right(albums);
        } on ServerException catch (e) {
          return Left(ServerFailure(e.errorCode));
        }
      },
    );
  }
}

