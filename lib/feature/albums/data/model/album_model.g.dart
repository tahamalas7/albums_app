// GENERATED CODE - DO NOT MODIFY BY HAND

part of album_model;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AlbumModel _$AlbumModelFromJson(Map<String, dynamic> json) {
  return AlbumModel(
    json['title'] as String,
    json['id'] as int,
  );
}

Map<String, dynamic> _$AlbumModelToJson(AlbumModel instance) =>
    <String, dynamic>{
      'title': instance.title,
      'id': instance.id,
    };
