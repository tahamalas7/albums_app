library album_model;

import 'package:albumsapp/feature/albums/domain/entity/album_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'album_model.g.dart';

@JsonSerializable()
class AlbumModel extends AlbumEntity {
  static const className = "AlbumModel";

  final String title;
  final int id;

  AlbumModel(
    this.title,
    this.id,
  );

  factory AlbumModel.fromJson(Map<String, dynamic> json) =>
      _$AlbumModelFromJson(json);
}
