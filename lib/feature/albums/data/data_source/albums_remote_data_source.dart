import 'package:albumsapp/core/data/base_remote_datasource.dart';
import 'package:albumsapp/core/util/constants.dart';
import 'package:albumsapp/feature/albums/data/model/album_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

abstract class AlbumsRemoteDataSource extends BaseRemoteDataSource {
  Future<List<AlbumModel>> getAlbums(int userID);
}

class AlbumsRemoteDataSourceImpl extends BaseRemoteDataSourceImpl
    implements AlbumsRemoteDataSource {
  final Dio dio;

  AlbumsRemoteDataSourceImpl({@required this.dio}) : super(dio: dio);

  @override
  Future<List<AlbumModel>> getAlbums(int userID) async {
    final albumsStringList =  await performGetRequest(
      Endpoints.getAlbums(userID),
    ) as List;
    return albumsStringList.map((e) => AlbumModel.fromJson(e)).toList();
  }
}
