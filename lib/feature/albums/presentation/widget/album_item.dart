import 'package:albumsapp/core/util/generate_screen.dart';
import 'package:albumsapp/feature/albums/domain/entity/album_entity.dart';
import 'package:flutter/material.dart';

class AlbumItem extends StatelessWidget {
  final AlbumEntity album;

  const AlbumItem({Key key, @required this.album}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(
          NameScreen.PHOTOS_PAGE,
          arguments: {
            'id': album.id,
            'title': album.title,
          },
        );
      },
      child: Center(
        child: Container(
          width: 280,
          margin: EdgeInsets.only(
            bottom: 60,
            top: 60,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30.0),
            color: Theme.of(context).primaryColor,
            boxShadow: [
              BoxShadow(
                offset: Offset(20, 20),
                color: Colors.black.withOpacity(0.3),
                blurRadius: 30,
              ),
              BoxShadow(
                offset: Offset(-20, -20),
                color: Colors.white,
                blurRadius: 30,
              ),
              BoxShadow(
                offset: Offset(0, 1),
                color: Theme.of(context).primaryColor,
                blurRadius: 30,
              ),
              BoxShadow(
                offset: Offset(0, -1),
                color: Theme.of(context).primaryColor,
                blurRadius: 30,
              ),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsetsDirectional.only(
                  start: 20.0,
                  end: 15,
                  top: 15,
                ),
                child: Text(
                  album.title,
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 180,
                      height: 130,
                      child: Icon(
                        Icons.photo,
                        color: Colors.grey,
                        size: 120,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsetsDirectional.only(
                  start: 15.0,
                  end: 15,
                  top: 20,
                  bottom: 20,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Album Details",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 192, 159, 1),
                          fontSize: 15,
                          fontFamily: "RobotoCondensed-Italic"),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        elevation: 10,
                        color: Theme.of(context).accentColor,
                        child: Container(
                          width: 20,
                          height: 20,
                          child: Center(
                            child: Icon(
                              Icons.arrow_forward_ios,
                              color: Colors.white,
                              size: 15,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
