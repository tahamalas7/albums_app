import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AlbumsAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 30,
      ),
      height: 150,
      width: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(
              "assets/images/appbar_background.png",
            ),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(50),
            bottomLeft: Radius.circular(50),
          ),
          color: Color(0xFFEAF3FA),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              offset: Offset(0, 10),
              blurRadius: 20,
            )
          ]),
      child: Stack(
        children: <Widget>[
          Container(
            height: 40,
            margin: EdgeInsets.only(top: 30),
            child: Text(
              'Albums page',
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Fluttertoast.showToast(
                msg: 'This feature is not implemented yet',
                gravity: ToastGravity.CENTER,
              );
            },
            child: Container(
              margin: EdgeInsets.only(top: 80),
              child: Container(
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(30.0),
                  ),
                  color: Color(0xFFEAF3FA),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.white,
                      blurRadius: 20,
                      offset: Offset(0, -10),
                    ),
                    BoxShadow(
                      color: Colors.black.withOpacity(0.2),
                      blurRadius: 20,
                      offset: Offset(10, 10),
                    ),
                  ],
                ),
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: TextField(
                      enabled: false,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        isDense: true,
                        focusedBorder: InputBorder.none,
                        hintText: 'Search in albums',
                        suffixIcon: Icon(
                          Icons.search,
                          size: 30,
                          color: Theme.of(context).accentColor,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
