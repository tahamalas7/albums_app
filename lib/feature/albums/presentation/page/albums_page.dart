import 'package:albumsapp/core/util/error_handler.dart';
import 'package:albumsapp/feature/albums/presentation/bloc/albums_bloc.dart';
import 'package:albumsapp/feature/albums/presentation/bloc/albums_state.dart';
import 'package:albumsapp/feature/albums/presentation/widget/album_item.dart';
import 'package:albumsapp/feature/albums/presentation/widget/albums_appbar.dart';
import 'package:albumsapp/injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AlbumsPage extends StatefulWidget {
  @override
  _AlbumsPageState createState() => _AlbumsPageState();
}

class _AlbumsPageState extends State<AlbumsPage> {
  final AlbumsBloc bloc = sl<AlbumsBloc>();

  @override
  void initState() {
    bloc.screenAppeared();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Stack(
        children: <Widget>[
          BlocListener(
            bloc: bloc,
            listener: (context, AlbumsState state) {
              if (state.errorCode != null) {
                ErrorHandler(context: context).showError(state.errorCode);
                bloc.resetError();
              }
            },
            child: BlocBuilder(
              builder: (context, AlbumsState state) {
                print('state is $state');
                return Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(
                        top: 130,
                      ),
                      child: ListView.builder(
                        itemCount: state.albums.length,
                        itemBuilder: (context, index) {
                          return AlbumItem(
                            album: state.albums[index],
                          );
                        },
                      ),
                    ),
                    if (state.isLoading)
                      Center(
                        child: CircularProgressIndicator(),
                      ),
                  ],
                );
              },
              bloc: bloc,
            ),
          ),
          AlbumsAppBar(),
        ],
      ),
    );
  }
}
