// GENERATED CODE - DO NOT MODIFY BY HAND

part of albums_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$AlbumsState extends AlbumsState {
  @override
  final BuiltList<AlbumEntity> albums;
  @override
  final bool isLoading;
  @override
  final ErrorCode errorCode;

  factory _$AlbumsState([void Function(AlbumsStateBuilder) updates]) =>
      (new AlbumsStateBuilder()..update(updates)).build();

  _$AlbumsState._({this.albums, this.isLoading, this.errorCode}) : super._() {
    if (albums == null) {
      throw new BuiltValueNullFieldError('AlbumsState', 'albums');
    }
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('AlbumsState', 'isLoading');
    }
  }

  @override
  AlbumsState rebuild(void Function(AlbumsStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AlbumsStateBuilder toBuilder() => new AlbumsStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AlbumsState &&
        albums == other.albums &&
        isLoading == other.isLoading &&
        errorCode == other.errorCode;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, albums.hashCode), isLoading.hashCode), errorCode.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AlbumsState')
          ..add('albums', albums)
          ..add('isLoading', isLoading)
          ..add('errorCode', errorCode))
        .toString();
  }
}

class AlbumsStateBuilder implements Builder<AlbumsState, AlbumsStateBuilder> {
  _$AlbumsState _$v;

  ListBuilder<AlbumEntity> _albums;
  ListBuilder<AlbumEntity> get albums =>
      _$this._albums ??= new ListBuilder<AlbumEntity>();
  set albums(ListBuilder<AlbumEntity> albums) => _$this._albums = albums;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  ErrorCode _errorCode;
  ErrorCode get errorCode => _$this._errorCode;
  set errorCode(ErrorCode errorCode) => _$this._errorCode = errorCode;

  AlbumsStateBuilder();

  AlbumsStateBuilder get _$this {
    if (_$v != null) {
      _albums = _$v.albums?.toBuilder();
      _isLoading = _$v.isLoading;
      _errorCode = _$v.errorCode;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AlbumsState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AlbumsState;
  }

  @override
  void update(void Function(AlbumsStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AlbumsState build() {
    _$AlbumsState _$result;
    try {
      _$result = _$v ??
          new _$AlbumsState._(
              albums: albums.build(),
              isLoading: isLoading,
              errorCode: errorCode);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'albums';
        albums.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'AlbumsState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
