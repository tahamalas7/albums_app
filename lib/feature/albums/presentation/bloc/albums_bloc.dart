import 'dart:async';

import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/core/util/constants.dart';
import 'package:albumsapp/feature/albums/domain/usecase/get_albums.dart';
import 'package:albumsapp/feature/albums/presentation/bloc/albums_event.dart';
import 'package:albumsapp/feature/albums/presentation/bloc/albums_state.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

class AlbumsBloc extends Bloc<AlbumsEvent, AlbumsState> {
  final GetAlbums getAlbums;

  AlbumsBloc({
    @required this.getAlbums,
  });

  void screenAppeared() {
    dispatch(ScreenAppeared());
  }

  void resetError() {
    dispatch(ResetError());
  }

  @override
  AlbumsState get initialState => AlbumsState.initial();

  @override
  Stream<AlbumsState> mapEventToState(AlbumsEvent event) async* {
    print('${event.toString()}');
    if (event is ScreenAppeared) {
      yield currentState.rebuild((builder) => builder..isLoading = true);
      final result = await getAlbums(GetAlbumsParams());
      yield* result.fold(
        (l) async* {
          if (l is ServerFailure)
            yield currentState.rebuild((builder) => builder
              ..errorCode = l.errorCode
              ..isLoading = false);
          else
            yield currentState.rebuild((builder) => builder
              ..errorCode = ErrorCode.SERVER_ERROR
              ..isLoading = false);
        },
        (r) async* {
          yield currentState.rebuild((builder) => builder
            ..albums.replace(r)
            ..isLoading = false);
        },
      );
    } else if (event is ResetError) {
      yield currentState.rebuild((builder) => builder
        ..isLoading = false
        ..errorCode = null);
    }
  }
}
