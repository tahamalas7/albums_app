library albums_state;

import 'dart:convert';

import 'package:albumsapp/core/util/constants.dart';
import 'package:albumsapp/feature/albums/domain/entity/album_entity.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'albums_state.g.dart';

abstract class AlbumsState implements Built<AlbumsState, AlbumsStateBuilder> {
  BuiltList<AlbumEntity> get albums;

  bool get isLoading;

  @nullable
  ErrorCode get errorCode;

  AlbumsState._();

  factory AlbumsState([updates(AlbumsStateBuilder b)]) = _$AlbumsState;

  factory AlbumsState.initial() {
    return AlbumsState((b) => b
      ..albums.replace([])
      ..isLoading = true);
  }
}
