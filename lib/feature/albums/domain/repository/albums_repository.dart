import 'package:albumsapp/core/data/base_repository.dart';
import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/feature/albums/domain/entity/album_entity.dart';
import 'package:dartz/dartz.dart';

abstract class AlbumsRepository extends BaseRepository {
  Future<Either<Failure, List<AlbumEntity>>> getAlbums();
}