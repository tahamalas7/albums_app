import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class AlbumEntity extends Equatable {
  final int id;
  final String title;

  AlbumEntity({
    @required this.id,
    @required this.title,
  }) : super([
          id,
          title,
        ]);
}
