import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/core/usecases/usecase.dart';
import 'package:albumsapp/feature/albums/domain/entity/album_entity.dart';
import 'package:albumsapp/feature/albums/domain/repository/albums_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class GetAlbums extends UseCase<List<AlbumEntity>, GetAlbumsParams> {
  final AlbumsRepository repository;

  GetAlbums({@required this.repository}) : assert(repository != null);

  @override
  Future<Either<Failure, List<AlbumEntity>>> call(
      GetAlbumsParams params) async {
    return await repository.getAlbums();
  }
}

class GetAlbumsParams extends Equatable {}
