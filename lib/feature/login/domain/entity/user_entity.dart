import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class UserEntity extends Equatable {
  final int id;
  final String name;

  UserEntity({@required this.id, @required this.name}) : super([id, name]);
}
