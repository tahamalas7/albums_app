import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/core/usecases/usecase.dart';
import 'package:albumsapp/feature/login/domain/entity/user_entity.dart';
import 'package:albumsapp/feature/login/domain/repository/login_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

///
/// The [Login] use case only call the loginUser function from the [LoginRepository] interface
///
class Login extends UseCase<UserEntity, LoginParams> {
  final LoginRepository repository;

  Login({@required this.repository}) : assert(repository != null);

  @override
  Future<Either<Failure, UserEntity>> call(LoginParams params) async {
    return await repository.loginUser(params.name);
  }
}

class LoginParams extends Equatable {
  final String name;

  LoginParams(this.name);
}
