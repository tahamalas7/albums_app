import 'package:albumsapp/core/data/base_repository.dart';
import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/feature/login/domain/entity/user_entity.dart';
import 'package:dartz/dartz.dart';

abstract class LoginRepository extends BaseRepository {

  Future<Either<Failure, UserEntity>> loginUser(
    String name,
  );
}
