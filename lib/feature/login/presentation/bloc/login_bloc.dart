import 'dart:async';

import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/core/util/constants.dart';
import 'package:albumsapp/core/util/input_validator.dart';
import 'package:albumsapp/feature/login/domain/usecase/login.dart';
import 'package:albumsapp/feature/login/presentation/bloc/login_event.dart';
import 'package:albumsapp/feature/login/presentation/bloc/login_state.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final Login login;
  final InputValidator inputValidator;

  LoginBloc({
    @required this.login,
    @required this.inputValidator,
  });

  void loginInitiated() {
    dispatch(LoginInitiated());
  }

  void changeName(String name) {
    dispatch(ChangeName(name));
  }

  void resetErrors() {
    dispatch(ResetError());
  }

  @override
  LoginState get initialState => LoginState.initial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    print('${event.toString()}');
    if (event is LoginInitiated) {
      yield* _loginInitiated(event);
    } else if (event is ChangeName) {
      yield* _checkName(event);
    } else if (event is ResetError) {
      yield currentState.rebuild((builder) => builder
        ..errorCode = null
        ..isSuccess = false);
    }
  }

  Stream<LoginState> _checkName(ChangeName event) async* {
    final inputEither = inputValidator.validateNameInput(event.name);
    yield* inputEither.fold(
      (failure) async* {
        yield currentState.rebuild((builder) => builder
          ..isNameValid = false
          ..name = event.name);
      },
      (name) async* {
        yield currentState.rebuild((builder) => builder
          ..name = event.name
          ..isNameValid = true);
      },
    );
  }

  Stream<LoginState> _loginInitiated(LoginInitiated event) async* {
    yield currentState.rebuild((builder) => builder..isLoading = true);
    final result = await login(LoginParams(currentState.name));
    yield* result.fold(
      (l) async* {
        if (l is ServerFailure)
          yield currentState.rebuild((builder) => builder
            ..errorCode = l.errorCode
            ..isLoading = false);
        else
          yield currentState.rebuild((builder) => builder
            ..errorCode = ErrorCode.NO_INTERNET_CONNECTION
            ..isLoading = false);
      },
      (r) async* {
        yield currentState.rebuild((builder) => builder
          ..isSuccess = true
          ..isLoading = false);
      },
    );
  }
}
