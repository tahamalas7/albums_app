// GENERATED CODE - DO NOT MODIFY BY HAND

part of login_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$LoginState extends LoginState {
  @override
  final String name;
  @override
  final bool isSuccess;
  @override
  final bool isLoading;
  @override
  final ErrorCode errorCode;
  @override
  final bool isNameValid;

  factory _$LoginState([void Function(LoginStateBuilder) updates]) =>
      (new LoginStateBuilder()..update(updates)).build();

  _$LoginState._(
      {this.name,
      this.isSuccess,
      this.isLoading,
      this.errorCode,
      this.isNameValid})
      : super._() {
    if (name == null) {
      throw new BuiltValueNullFieldError('LoginState', 'name');
    }
    if (isSuccess == null) {
      throw new BuiltValueNullFieldError('LoginState', 'isSuccess');
    }
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('LoginState', 'isLoading');
    }
  }

  @override
  LoginState rebuild(void Function(LoginStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoginStateBuilder toBuilder() => new LoginStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LoginState &&
        name == other.name &&
        isSuccess == other.isSuccess &&
        isLoading == other.isLoading &&
        errorCode == other.errorCode &&
        isNameValid == other.isNameValid;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, name.hashCode), isSuccess.hashCode),
                isLoading.hashCode),
            errorCode.hashCode),
        isNameValid.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('LoginState')
          ..add('name', name)
          ..add('isSuccess', isSuccess)
          ..add('isLoading', isLoading)
          ..add('errorCode', errorCode)
          ..add('isNameValid', isNameValid))
        .toString();
  }
}

class LoginStateBuilder implements Builder<LoginState, LoginStateBuilder> {
  _$LoginState _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  bool _isSuccess;
  bool get isSuccess => _$this._isSuccess;
  set isSuccess(bool isSuccess) => _$this._isSuccess = isSuccess;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  ErrorCode _errorCode;
  ErrorCode get errorCode => _$this._errorCode;
  set errorCode(ErrorCode errorCode) => _$this._errorCode = errorCode;

  bool _isNameValid;
  bool get isNameValid => _$this._isNameValid;
  set isNameValid(bool isNameValid) => _$this._isNameValid = isNameValid;

  LoginStateBuilder();

  LoginStateBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _isSuccess = _$v.isSuccess;
      _isLoading = _$v.isLoading;
      _errorCode = _$v.errorCode;
      _isNameValid = _$v.isNameValid;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoginState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$LoginState;
  }

  @override
  void update(void Function(LoginStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$LoginState build() {
    final _$result = _$v ??
        new _$LoginState._(
            name: name,
            isSuccess: isSuccess,
            isLoading: isLoading,
            errorCode: errorCode,
            isNameValid: isNameValid);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
