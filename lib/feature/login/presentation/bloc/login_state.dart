library login_state;

import 'package:albumsapp/core/util/constants.dart';
import 'package:built_value/built_value.dart';

part 'login_state.g.dart';

abstract class LoginState implements Built<LoginState, LoginStateBuilder> {
  String get name;

  bool get isSuccess;

  bool get isLoading;

  @nullable
  ErrorCode get errorCode;

  @nullable
  bool get isNameValid;

  LoginState._();

  factory LoginState([updates(LoginStateBuilder b)]) = _$LoginState;

  factory LoginState.initial() {
    return LoginState((b) => b
      ..name = ''
      ..isLoading = false
      ..isSuccess = false);
  }
}
