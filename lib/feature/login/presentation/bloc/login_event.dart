abstract class LoginEvent {}

class LoginInitiated extends LoginEvent {
  LoginInitiated();
}

class ChangeName extends LoginEvent {
  final String name;

  ChangeName(this.name);
}

class ResetError extends LoginEvent {}
