import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final Function(String) onChange;
  final bool isError;
  final String hintText;

  const CustomTextField({
    Key key,
    @required this.onChange,
    @required this.isError,
    @required this.hintText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsetsDirectional.only(start: 20.0, end: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(30)),
        border: Border.all(
          color: Theme.of(context).accentColor,
          width: 3,
        ),
      ),
      height: 43,
      child: Center(
        child: TextField(
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(
              fontSize: 18.0,
            ),
            decoration: InputDecoration(
              isDense: true,
              contentPadding: EdgeInsets.zero,
              icon: Icon(
                Icons.email,
                color: Theme.of(context).textTheme.bodyText1.color,
              ),
              focusedBorder: InputBorder.none,
              hintText: hintText,
              hintStyle: TextStyle(
                color: Theme.of(context).textTheme.bodyText1.color,
                fontSize: 18.0,
              ),
              labelStyle: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.normal,
                color: Theme.of(context).textTheme.bodyText1.color,
              ),
              disabledBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
            ),
            onChanged: (text) => onChange(text)),
      ),
    );
  }
}
