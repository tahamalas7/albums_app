import 'package:albumsapp/core/util/error_handler.dart';
import 'package:albumsapp/core/util/generate_screen.dart';
import 'package:albumsapp/feature/login/presentation/bloc/login_bloc.dart';
import 'package:albumsapp/feature/login/presentation/bloc/login_state.dart';
import 'package:albumsapp/feature/login/presentation/widget/custom_button.dart';
import 'package:albumsapp/feature/login/presentation/widget/custom_text_field.dart';
import 'package:albumsapp/injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _bloc = sl<LoginBloc>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _bloc,
      listener: (BuildContext context, LoginState state) {
        if (state.isSuccess) {
          Navigator.of(context).pushReplacementNamed(NameScreen.ALBUMS_PAGE);
          _bloc.resetErrors();
        }
        if (state.errorCode != null) {
          ErrorHandler(context: context).showError(state.errorCode);
          _bloc.resetErrors();
        }
      },
      child: BlocBuilder(
        bloc: _bloc,
        builder: (context, LoginState state) {
          return Scaffold(
            body: Stack(
              children: <Widget>[
                Image.asset(
                  "assets/images/background.png",
                  width: double.infinity,
                  height: double.infinity,
                  fit: BoxFit.fill,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'LOGO',
                      style: TextStyle(
                        fontSize: 32,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: Text(
                              "Welcome Back!",
                              style: TextStyle(
                                fontSize: 32,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Text(
                            "Login with your account",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 30.0),

                      child: Container(
                        margin:  EdgeInsetsDirectional.only(start: 20.0, end: 20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            CustomTextField(
                              hintText: 'Name',
                              onChange: (name) {
                                _bloc.changeName(name);
                              },
                              isError: state.isNameValid ?? false,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: AnimatedOpacity(
                                opacity: state.isNameValid ?? true ? 0 : 1,
                                duration: Duration(milliseconds: 200),
                                child: Text(
                                  'Please enter a valid name',
                                  style: TextStyle(
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsetsDirectional.only(
                        start: 20.0,
                        end: 20,
                        top: 40,
                      ),
                      child: CustomButton(
                        title: 'Login',
                        onClick: _bloc.loginInitiated,
                        isEnabled:
                            state.isNameValid ?? false && !state.isLoading,
                      ),
                    ),
                  ],
                ),
                if (state.isLoading)
                  Center(
                    child: CircularProgressIndicator(),
                  ),
              ],
            ),
          );
        },
      ),
    );
  }
}
