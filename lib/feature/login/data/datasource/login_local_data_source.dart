import 'package:albumsapp/core/data/base_local_data_source.dart';
import 'package:albumsapp/core/data/shared_preferences_keys.dart';
import 'package:albumsapp/feature/login/data/model/user_model.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class LoginLocalDataSource extends BaseLocalDataSource {
  /// This function is to register the user locally, it stores it in the [SharedPreferences]
  /// Output:
  ///    It does not return anything
  ///
  Future<void> registerUserLocally(UserModel userModel);
}

class LoginLocalDataSourceImpl extends BaseLocalDataSourceImpl
    implements LoginLocalDataSource {
  LoginLocalDataSourceImpl({@required SharedPreferences sharedPreferences})
      : super(
          sharedPreferences: sharedPreferences,
        );

  @override
  Future<void> registerUserLocally(UserModel userModel) {
    // Storing the user name in SharedPreferences to use it later
    sharedPreferences.setString(SharedPreferencesKeys.USERNAME, userModel.name);
    // Storing the id in SharedPreferences to use it later
    sharedPreferences.setInt(SharedPreferencesKeys.ID, userModel.id);
    // Storing that the user is logged in in SharedPreferences to use it later
    sharedPreferences.setBool(SharedPreferencesKeys.IS_LOGGED_IN, true);
    return Future.value();
  }
}
