import 'package:albumsapp/core/data/base_remote_datasource.dart';
import 'package:albumsapp/core/error/exceptions.dart';
import 'package:albumsapp/core/util/constants.dart';
import 'package:albumsapp/feature/login/data/model/user_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

abstract class LoginRemoteDataSource extends BaseRemoteDataSource {

  /// This function is to login the user into the system
  /// Output: Either
  ///  1- The [UserModel] of the entered user
  ///  2- Throws exception with error code [ErrorCode.WRONG_INPUT] if the user does not exist
  Future<UserModel> loginUser(String name);
}

class LoginRemoteDataSourceImpl extends BaseRemoteDataSourceImpl
    implements LoginRemoteDataSource {
  final Dio dio;

  LoginRemoteDataSourceImpl({@required this.dio}) : super(dio: dio);


  @override
  Future<UserModel> loginUser(String name) async {

    // Performing the HTTP GET request
    final response = await performGetRequest(
      Endpoints.LOGIN,
    );

    print('resposne is ${response is List<Map<String, dynamic>>}');


    // Parsing the list of users
    final usersList = (response as List)
        .map((e) => UserModel.fromJson(e));

    print('users list $usersList');

    // Checking if the entered user name is one of the existing users from the remote data source
    return usersList.firstWhere(
      (element) => element.name == name,
      orElse: () {
        throw ServerException(ErrorCode.WRONG_INPUT);
      },
    );
  }
}
