library user_model;

import 'dart:convert';

import 'package:albumsapp/feature/login/domain/entity/user_entity.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';


@JsonSerializable()
class UserModel extends UserEntity {
  static const className = "UserModel";

  @JsonKey(name: 'id')
  final int id;
  final String name;

  UserModel(this.id, this.name);

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);
}
