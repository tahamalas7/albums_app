import 'package:albumsapp/core/data/base_repository.dart';
import 'package:albumsapp/core/error/exceptions.dart';
import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/core/network/network_info.dart';
import 'package:albumsapp/feature/login/data/datasource/login_local_data_source.dart';
import 'package:albumsapp/feature/login/data/datasource/login_remote_data_source.dart';
import 'package:albumsapp/feature/login/domain/entity/user_entity.dart';
import 'package:albumsapp/feature/login/domain/repository/login_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

class LoginRepositoryImpl extends BaseRepositoryImpl
    implements LoginRepository {
  final LoginRemoteDataSource loginRemoteDataSource;
  final LoginLocalDataSource loginLocalDataSource;

  LoginRepositoryImpl({
    @required this.loginRemoteDataSource,
    @required this.loginLocalDataSource,
    @required NetworkInfo networkInfo,
  }) : super(
          networkInfo: networkInfo,
          baseLocalDataSource: loginLocalDataSource,
        );

  ///
  /// The loginUser function is responsible for checking the user's credentials as well as storing the user's data in the local data source
  /// Output:
  ///   Either a [UserEntity] or [ServerFailure] stating the issue that occurred with [ErrorCodes] in it to determine the issue
  ///
  @override
  Future<Either<Failure, UserEntity>> loginUser(String name) async {
    return await checkNetwork<UserEntity>(
      () async {
        try {
          final userResponse = await loginRemoteDataSource.loginUser(name);
          if (userResponse != null) {
            await loginLocalDataSource.registerUserLocally(userResponse);
          }
          return Right(userResponse);
        } on ServerException catch (e) {
          return Left(ServerFailure(e.errorCode));
        }
      },
    );
  }
}
