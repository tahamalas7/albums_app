import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class LocalPhotoEntity extends Equatable {
  final String thumbnail;
  final String title;

  LocalPhotoEntity({
    @required this.thumbnail,
    @required this.title,
  }) : super([
          thumbnail,
        ]);
}
