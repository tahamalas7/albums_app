import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class PhotoEntity extends Equatable {
  final int id;
  final String thumbnail;
  String title;
  final String url;

  PhotoEntity({
    @required this.id,
    @required this.thumbnail,
    @required this.url,
    @required this.title,
  }) : super([
          id,
          thumbnail,
          url,
          title,
        ]);
}
