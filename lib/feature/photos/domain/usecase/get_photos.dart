import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/core/usecases/usecase.dart';
import 'package:albumsapp/feature/photos/domain/entity/photo_entity.dart';
import 'package:albumsapp/feature/photos/domain/repository/photos_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class GetPhotos extends UseCase<List<PhotoEntity>, GetPhotosParams> {
  final PhotosRepository repository;

  GetPhotos({@required this.repository}) : assert(repository != null);

  @override
  Future<Either<Failure, List<PhotoEntity>>> call(
      GetPhotosParams params) async {
    return await repository.getPhotos(params.albumID);
  }
}

class GetPhotosParams extends Equatable {
  final int albumID;

  GetPhotosParams({
    @required this.albumID,
  }) : super([
          albumID,
        ]);
}
