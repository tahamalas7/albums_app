import 'package:albumsapp/core/data/base_repository.dart';
import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/feature/photos/domain/entity/photo_entity.dart';
import 'package:dartz/dartz.dart';

abstract class PhotosRepository extends BaseRepository {
  Future<Either<Failure, List<PhotoEntity>>> getPhotos(int albumID);
}
