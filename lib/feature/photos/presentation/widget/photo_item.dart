import 'dart:io';

import 'package:albumsapp/feature/photos/domain/entity/photo_entity.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class PhotoItem extends StatelessWidget {
  final PhotoEntity photo;
  final Function(String newTitle) itemEdited;
  String imageTitle = '';

  PhotoItem({
    Key key,
    @required this.photo,
    @required this.itemEdited,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsets.only(
          left: 20,
          right: 20,
          bottom: 60,
          top: 60,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          color: Theme.of(context).primaryColor,
          boxShadow: [
            BoxShadow(
              offset: Offset(20, 20),
              color: Colors.black.withOpacity(0.3),
              blurRadius: 30,
            ),
            BoxShadow(
              offset: Offset(-20, -20),
              color: Colors.white,
              blurRadius: 30,
            ),
            BoxShadow(
              offset: Offset(0, 1),
              color: Theme.of(context).primaryColor,
              blurRadius: 30,
            ),
            BoxShadow(
              offset: Offset(0, -1),
              color: Theme.of(context).primaryColor,
              blurRadius: 30,
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsetsDirectional.only(
                start: 20.0,
                end: 15,
                top: 15,
              ),
              child: Text(
                photo.title,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Center(
                child: CachedNetworkImage(
                  imageUrl: photo.thumbnail,
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                showEditDialog(context);
              },
              child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsetsDirectional.only(
                    start: 15.0,
                    end: 15,
                    top: 20,
                    bottom: 20,
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        "Edit",
                        style: TextStyle(
                            color: Color.fromRGBO(255, 192, 159, 1),
                            fontSize: 15,
                            fontFamily: "RobotoCondensed-Italic"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          elevation: 10,
                          color: Theme.of(context).accentColor,
                          child: Container(
                            width: 20,
                            height: 20,
                            child: Center(
                              child: Icon(
                                Icons.edit,
                                color: Colors.white,
                                size: 15,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void showEditDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(
            'Edit image',
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: SingleChildScrollView(
            child: TextField(
              decoration: InputDecoration(
                hintText: 'New image title',
              ),
              onChanged: (newTitle) {
                imageTitle = newTitle;
              },
            ),
          ),
          actions: [
            GestureDetector(
              onTap: () {
                imageTitle = '';
                Navigator.pop(context);
              },
              child: Container(
                height: 40,
                width: 80,
                decoration: BoxDecoration(
                  borderRadius: new BorderRadius.all(new Radius.circular(30.0)),
                  border: new Border.all(
                    color: Theme.of(context).primaryColor,
                    width: 1.0,
                  ),
                ),
                child: Center(
                  child: Text(
                    'Cancel',
                    style: TextStyle(
                      color: Theme.of(context).accentColor,
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                itemEdited(imageTitle);
                imageTitle = '';
                Navigator.pop(context);
              },
              child: Container(
                height: 40,
                width: 80,
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: new BorderRadius.all(new Radius.circular(30.0)),
                ),
                child: Center(
                  child: Text(
                    'Edit',
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
