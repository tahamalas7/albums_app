import 'dart:io';

import 'package:custom_image_picker/custom_image_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

class PickImage extends StatefulWidget {
  final ScrollController controller;
  final Function(String imageTitle, String filePath) itemAdded;

  const PickImage({
    Key key,
    @required this.controller,
    @required this.itemAdded,
  }) : super(key: key);

  @override
  _PickImageState createState() => _PickImageState();
}

class _PickImageState extends State<PickImage> {
  List<PhoneAlbum> albums = [];
  List<PhonePhoto> images = [];
  String imageTitle = '';
  PageController controller = PageController();
  final CustomImagePicker customImagePicker = CustomImagePicker();
  int page = 1;
  PhoneAlbum selectedPhoneAlbum;

  @override
  void initState() {
    print('initiated');
    widget.controller.addListener(() {
      if (selectedPhoneAlbum != null) {
        if (widget.controller.position.pixels ==
            widget.controller.position.maxScrollExtent) {
          print('get new images $page');
          page++;
          getImagesOfGallery();
        }
      }
    });
    customImagePicker.getAlbums(callback: (msg) {
      setState(() {
        albums = msg;
      });
    });
    super.initState();
  }

  Future<void> getImagesOfGallery() async {
    try {
      await customImagePicker.getPhotosOfAlbum(selectedPhoneAlbum.id, page: page,
          callback: (msg) {
            print('the message is $msg');
            setState(() {
              images.addAll(msg);
            });
          });
    } on PlatformException {}
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(40),
          topRight: Radius.circular(40),
        ),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 1),
            color: Theme.of(context).primaryColor,
            blurRadius: 30,
          ),
          BoxShadow(
            offset: Offset(0, -1),
            color: Theme.of(context).primaryColor,
            blurRadius: 30,
          ),
          BoxShadow(
            offset: Offset(0, -20),
            blurRadius: 30,
            color: Colors.white,
          ),
        ],
      ),
      child: PageView(
        controller: controller,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          ListView.builder(
            itemBuilder: (context, int index) {
              return ListTile(
                onTap: () async {
                  controller.nextPage(
                    duration: Duration(milliseconds: 200),
                    curve: Curves.ease,
                  );
                  setState(() {
                    page = 1;
                    images.clear();
                    selectedPhoneAlbum = albums[index];
                    getImagesOfGallery();
                  });
                },
                title: Text(
                  albums[index].name,
                  style: TextStyle(color: Colors.blueGrey),
                ),
                subtitle: Text(
                  albums[index].photosCount.toString(),
                  style: TextStyle(color: Colors.grey.withAlpha(200)),
                ),
                leading: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: FileImage(
                        File(albums[index].coverUri),
                      ),
                    ),
                  ),
                ),
              );
            },
            itemCount: albums.length,
          ),
          Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  backIcon(),
                  Expanded(
                    child: GridView.builder(
                      itemBuilder: (context, index) {
                        return photoItem(
                          images[index].photoUri,
                        );
                      },
                      itemCount: images.length,
                      physics: NeverScrollableScrollPhysics(),
                      controller: widget.controller,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                      ),
                    ),
                  ),
                ],
              ),
              if (images.isEmpty)
                Center(
                  child: CircularProgressIndicator(),
                ),
            ],
          ),
        ],
      ),
    );
  }

  Widget photoItem(String filePath) {
    return GestureDetector(
      onTap: () {
        showAddDialog(filePath, context);
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Image.file(
          File(
            filePath,
          ),
        ),
      ),
    );
  }

  void showAddDialog(String imagePath, BuildContext context) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(
            'Add image',
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
          ),
          content: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.file(
                    File(
                      imagePath,
                    ),
                  ),
                ),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Image name',
                  ),
                  onChanged: (newTitle) {
                    imageTitle = newTitle;
                  },
                ),
              ],
            ),
          ),
          actions: [
            GestureDetector(
              onTap: () {
                imageTitle = '';
                Navigator.pop(context);
              },
              child: Container(
                height: 40,
                width: 80,
                decoration: BoxDecoration(
                  borderRadius: new BorderRadius.all(new Radius.circular(30.0)),
                  border: new Border.all(
                    color: Theme.of(context).primaryColor,
                    width: 1.0,
                  ),
                ),
                child: Center(
                  child: Text(
                    'Cancel',
                    style: TextStyle(
                      color: Theme.of(context).accentColor,
                      fontFamily: 'Regular',
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                widget.itemAdded(imageTitle, imagePath);
                imageTitle = '';
                Navigator.pop(context);
              },
              child: Container(
                height: 40,
                width: 80,
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: new BorderRadius.all(new Radius.circular(30.0)),
                ),
                child: Center(
                  child: Text(
                    'Add',
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget backIcon() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(
            top: 40.0,
            left: 10,
            right: 10,
          ),
          child: Center(
            child: Container(
              width: 30,
              height: 30,
              padding: const EdgeInsets.all(3.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Theme.of(context).primaryColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.white,
                    offset: Offset(-10, -10),
                    blurRadius: 20,
                  ),
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    offset: Offset(10, 10),
                    blurRadius: 20,
                  ),
                ],
              ),
              child: GestureDetector(
                onTap: () {
                  controller.previousPage(
                      duration: Duration(milliseconds: 200),
                      curve: Curves.ease);
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Theme.of(context).accentColor,
                  size: 20,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
