import 'dart:async';

import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/core/util/constants.dart';
import 'package:albumsapp/feature/photos/domain/entity/local_photo_entity.dart';
import 'package:albumsapp/feature/photos/domain/entity/photo_entity.dart';
import 'package:albumsapp/feature/photos/domain/usecase/get_photos.dart';
import 'package:albumsapp/feature/photos/presentation/bloc/photos_event.dart';
import 'package:albumsapp/feature/photos/presentation/bloc/photos_state.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

class PhotosBloc extends Bloc<PhotosEvent, PhotosState> {
  final GetPhotos getPhotos;

  PhotosBloc({
    @required this.getPhotos,
  });

  void screenAppeared(int albumID) {
    dispatch(ScreenAppeared(albumID));
  }

  void resetErrors() {
    dispatch(ResetError());
  }

  void addLocalItem(String imageTitle, String imagePath) {
    dispatch(AddLocalItem(imageTitle, imagePath));
  }

  void imageEdited(String newTitle, int index) {
    dispatch(ImageEdited(newTitle, index));
  }

  void localImageEdited(String newTitle, int index) {
    dispatch(LocalImageEdited(newTitle, index));
  }

  @override
  PhotosState get initialState => PhotosState.initial();

  @override
  Stream<PhotosState> mapEventToState(PhotosEvent event) async* {
    print('${event.toString()}');
    if (event is ScreenAppeared) {
      yield currentState.rebuild((builder) => builder..isLoading = true);
      final result = await getPhotos(GetPhotosParams(albumID: event.albumID));
      yield* result.fold(
        (l) async* {
          if (l is ServerFailure)
            yield currentState.rebuild((builder) => builder
              ..errorCode = l.errorCode
              ..isLoading = false);
          else
            yield currentState.rebuild((builder) => builder
              ..errorCode = ErrorCode.SERVER_ERROR
              ..isLoading = false);
        },
        (r) async* {
          yield currentState.rebuild((builder) => builder
            ..photos.replace(r)
            ..isLoading = false);
        },
      );
    } else if (event is ResetError) {
      yield currentState.rebuild((builder) => builder
        ..isLoading = false
        ..errorCode = null);
    } else if (event is AddLocalItem) {
      yield currentState.rebuild(
        (builder) => builder
          ..localPhotos.add(
            LocalPhotoEntity(
              thumbnail: event.filePath,
              title: event.imageTitle,
            ),
          ),
      );
    } else if (event is LocalImageEdited) {
      try {
        var list = currentState.localPhotos;
        var item = list[event.index];
        var newItem = LocalPhotoEntity(
            thumbnail: item.thumbnail, title: event.imageTitle);
        list = list.rebuild((b) => b..removeAt(event.index));
        list = list.rebuild((b) => b..insert(event.index, newItem));
        print('list ${list[event.index].title}');
        yield currentState
            .rebuild((builder) => builder..localPhotos.replace(list));
      } catch (e) {
        print('e $e');
      }
    } else if (event is ImageEdited) {
      var list = currentState.photos;
      var item = list[event.index];
      var newItem = PhotoEntity(
        thumbnail: item.thumbnail,
        title: event.imageTitle,
        id: item.id,
        url: item.url,
      );
      list = list.rebuild((b) => b..removeAt(event.index));
      list = list.rebuild((b) => b..insert(event.index, newItem));
      yield currentState.rebuild((builder) => builder..photos.replace(list));
    }
  }
}
