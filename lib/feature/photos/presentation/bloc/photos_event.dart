abstract class PhotosEvent {}

class ScreenAppeared extends PhotosEvent {
  final int albumID;

  ScreenAppeared(this.albumID);
}

class ResetError extends PhotosEvent {}

class AddLocalItem extends PhotosEvent {
  final String imageTitle;
  final String filePath;

  AddLocalItem(this.imageTitle, this.filePath);
}

class ImageEdited extends PhotosEvent {
  final String imageTitle;
  final int index;

  ImageEdited(this.imageTitle, this.index);
}

class LocalImageEdited extends PhotosEvent {
  final String imageTitle;
  final int index;

  LocalImageEdited(this.imageTitle, this.index);
}
