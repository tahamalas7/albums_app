library photos_state;

import 'package:albumsapp/core/util/constants.dart';
import 'package:albumsapp/feature/photos/domain/entity/local_photo_entity.dart';
import 'package:albumsapp/feature/photos/domain/entity/photo_entity.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

part 'photos_state.g.dart';

abstract class PhotosState implements Built<PhotosState, PhotosStateBuilder> {
  bool get isLoading;

  BuiltList<PhotoEntity> get photos;

  BuiltList<LocalPhotoEntity> get localPhotos;

  @nullable
  ErrorCode get errorCode;

  PhotosState._();

  factory PhotosState([updates(PhotosStateBuilder b)]) = _$PhotosState;

  factory PhotosState.initial() {
    return PhotosState((b) => b
      ..localPhotos.replace([])
      ..isLoading = true
      ..photos.replace([]));
  }
}
