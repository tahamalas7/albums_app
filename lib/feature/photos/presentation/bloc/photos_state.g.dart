// GENERATED CODE - DO NOT MODIFY BY HAND

part of photos_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PhotosState extends PhotosState {
  @override
  final bool isLoading;
  @override
  final BuiltList<PhotoEntity> photos;
  @override
  final BuiltList<LocalPhotoEntity> localPhotos;
  @override
  final ErrorCode errorCode;

  factory _$PhotosState([void Function(PhotosStateBuilder) updates]) =>
      (new PhotosStateBuilder()..update(updates)).build();

  _$PhotosState._(
      {this.isLoading, this.photos, this.localPhotos, this.errorCode})
      : super._() {
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('PhotosState', 'isLoading');
    }
    if (photos == null) {
      throw new BuiltValueNullFieldError('PhotosState', 'photos');
    }
    if (localPhotos == null) {
      throw new BuiltValueNullFieldError('PhotosState', 'localPhotos');
    }
  }

  @override
  PhotosState rebuild(void Function(PhotosStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PhotosStateBuilder toBuilder() => new PhotosStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PhotosState &&
        isLoading == other.isLoading &&
        photos == other.photos &&
        localPhotos == other.localPhotos &&
        errorCode == other.errorCode;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, isLoading.hashCode), photos.hashCode),
            localPhotos.hashCode),
        errorCode.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PhotosState')
          ..add('isLoading', isLoading)
          ..add('photos', photos)
          ..add('localPhotos', localPhotos)
          ..add('errorCode', errorCode))
        .toString();
  }
}

class PhotosStateBuilder implements Builder<PhotosState, PhotosStateBuilder> {
  _$PhotosState _$v;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  ListBuilder<PhotoEntity> _photos;
  ListBuilder<PhotoEntity> get photos =>
      _$this._photos ??= new ListBuilder<PhotoEntity>();
  set photos(ListBuilder<PhotoEntity> photos) => _$this._photos = photos;

  ListBuilder<LocalPhotoEntity> _localPhotos;
  ListBuilder<LocalPhotoEntity> get localPhotos =>
      _$this._localPhotos ??= new ListBuilder<LocalPhotoEntity>();
  set localPhotos(ListBuilder<LocalPhotoEntity> localPhotos) =>
      _$this._localPhotos = localPhotos;

  ErrorCode _errorCode;
  ErrorCode get errorCode => _$this._errorCode;
  set errorCode(ErrorCode errorCode) => _$this._errorCode = errorCode;

  PhotosStateBuilder();

  PhotosStateBuilder get _$this {
    if (_$v != null) {
      _isLoading = _$v.isLoading;
      _photos = _$v.photos?.toBuilder();
      _localPhotos = _$v.localPhotos?.toBuilder();
      _errorCode = _$v.errorCode;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PhotosState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PhotosState;
  }

  @override
  void update(void Function(PhotosStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PhotosState build() {
    _$PhotosState _$result;
    try {
      _$result = _$v ??
          new _$PhotosState._(
              isLoading: isLoading,
              photos: photos.build(),
              localPhotos: localPhotos.build(),
              errorCode: errorCode);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'photos';
        photos.build();
        _$failedField = 'localPhotos';
        localPhotos.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PhotosState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
