import 'package:albumsapp/core/util/error_handler.dart';
import 'package:albumsapp/feature/photos/presentation/bloc/photos_bloc.dart';
import 'package:albumsapp/feature/photos/presentation/bloc/photos_state.dart';
import 'package:albumsapp/feature/photos/presentation/widget/local_photo_item.dart';
import 'package:albumsapp/feature/photos/presentation/widget/photo_item.dart';
import 'package:albumsapp/feature/photos/presentation/widget/pick_image.dart';
import 'package:albumsapp/injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rubber/rubber.dart';

class PhotosPage extends StatefulWidget {
  final int albumID;
  final String albumTitle;

  const PhotosPage({Key key, @required this.albumID, @required this.albumTitle})
      : super(key: key);

  @override
  _PhotosPageState createState() => _PhotosPageState();
}

class _PhotosPageState extends State<PhotosPage> with TickerProviderStateMixin {
  final bloc = sl<PhotosBloc>();
  RubberAnimationController _rubberController;
  AnimationController _animationController;
  ScrollController _controller = ScrollController();
  bool isShown = false;

  @override
  void initState() {
    bloc.screenAppeared(widget.albumID);
    _rubberController = RubberAnimationController(
      duration: Duration(milliseconds: 200),
      vsync: this,
      lowerBoundValue: AnimationControllerValue(percentage: 0),
      halfBoundValue: AnimationControllerValue(percentage: 0.5),
      upperBoundValue: AnimationControllerValue(percentage: 0.8),
    );
    _rubberController.addListener(() {
      try {
        if (_rubberController.isCompleted &&
            _rubberController
                    .getBoundFromState(_rubberController.animationState.value) <
                0.4) _animationController.reverse();
      } catch (e) {}
    });
    _animationController = AnimationController(
      vsync: this,
      duration: new Duration(milliseconds: 200),
    );
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      floatingActionButton: FloatingActionButton(
        child: AnimatedBuilder(
          animation: _animationController,
          builder: (BuildContext context, Widget _widget) {
            print('_animationController.value ${_animationController.value}');
            return new Transform.rotate(
              angle: _animationController.value > 0.8
                  ? 0.8
                  : _animationController.value,
              child: _widget,
            );
          },
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
        ),
        backgroundColor: Theme.of(context).accentColor,
        onPressed: () {
          if (!isShown)
            setState(() {
              isShown = true;
              _animationController.forward();
              _rubberController.halfExpand();
            });
          else
            setState(() {
              isShown = false;
              _animationController.reverse();
              _rubberController.collapse();
            });
        },
      ),
      body: RubberBottomSheet(
        upperLayer: PickImage(
          controller: _controller,
          itemAdded: (String imageTitle, String filePath) {
            _animationController.reverse();
            _rubberController.collapse();
            bloc.addLocalItem(imageTitle, filePath);
          },
        ),
        scrollController: _controller,
        headerHeight: 0,
        animationController: _rubberController,
        lowerLayer: Stack(
          children: <Widget>[
            BlocListener(
              bloc: bloc,
              listener: (context, PhotosState state) {
                if (state.errorCode != null) {
                  ErrorHandler(context: context).showError(state.errorCode);
                  bloc.resetErrors();
                }
              },
              child: BlocBuilder(
                builder: (context, PhotosState state) {
                  print('state is $state');
                  return Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 80),
                        child: Column(
                          children: <Widget>[
                            if (state.photos.isNotEmpty)
                              Text(
                                widget.albumTitle,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 30,
                                ),
                              ),
                            if (state.photos.isNotEmpty)
                              Text(
                                '${state.photos.length + state.localPhotos.length} items',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(top: 20),
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: state.photos.length +
                                      state.localPhotos.length,
                                  itemBuilder: (context, index) {
                                    if (index >= state.localPhotos.length) {
                                      return PhotoItem(
                                        photo: state.photos[
                                            index - state.localPhotos.length],
                                        itemEdited: (newTitle) {
                                          bloc.imageEdited(newTitle,
                                              index - state.localPhotos.length);
                                        },
                                      );
                                    } else
                                      return LocalPhotoItem(
                                        photo: state.localPhotos[index],
                                        itemEdited: (newTitle) {
                                          bloc.localImageEdited(
                                              newTitle, index);
                                        },
                                      );
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      if (state.isLoading)
                        Center(
                          child: CircularProgressIndicator(),
                        ),
                    ],
                  );
                },
                bloc: bloc,
              ),
            ),
            backIcon(),
          ],
        ),
      ),
    );
  }

  Widget backIcon() {
    return Positioned(
      top: 0,
      right: 0,
      left: 0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
              top: 40.0,
              left: 10,
              right: 10,
            ),
            child: Center(
              child: Container(
                width: 30,
                height: 30,
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Theme.of(context).primaryColor,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.white,
                      offset: Offset(-10, -10),
                      blurRadius: 20,
                    ),
                    BoxShadow(
                      color: Colors.black.withOpacity(0.2),
                      offset: Offset(10, 10),
                      blurRadius: 20,
                    ),
                  ],
                ),
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Theme.of(context).accentColor,
                    size: 20,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
