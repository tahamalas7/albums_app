// GENERATED CODE - DO NOT MODIFY BY HAND

part of photo_model;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhotoModel _$PhotoModelFromJson(Map<String, dynamic> json) {
  return PhotoModel(
    json['id'] as int,
    json['url'] as String,
    json['thumbnailUrl'] as String,
    json['title'] as String,
  );
}

Map<String, dynamic> _$PhotoModelToJson(PhotoModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'thumbnailUrl': instance.thumbnail,
      'url': instance.url,
      'title': instance.title,
    };
