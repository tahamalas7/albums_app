library photo_model;

import 'package:albumsapp/feature/photos/domain/entity/photo_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'photo_model.g.dart';

@JsonSerializable()
class PhotoModel extends PhotoEntity {
  static const className = "PhotoModel";

  final int id;
  @JsonKey(name: 'thumbnailUrl')
  final String thumbnail;
  final String url;
  final String title;

  PhotoModel(
    this.id,
    this.url,
    this.thumbnail,
    this.title,
  );

  factory PhotoModel.fromJson(Map<String, dynamic> json) =>
      _$PhotoModelFromJson(json);
}
