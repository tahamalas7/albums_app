import 'package:albumsapp/core/data/base_remote_datasource.dart';
import 'package:albumsapp/core/util/constants.dart';
import 'package:albumsapp/feature/photos/data/model/photo_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

abstract class PhotosRemoteDataSource extends BaseRemoteDataSource {
  Future<List<PhotoModel>> getPhotos(int albumID);
}

class PhotosRemoteDataSourceImpl extends BaseRemoteDataSourceImpl
    implements PhotosRemoteDataSource {
  final Dio dio;

  PhotosRemoteDataSourceImpl({@required this.dio}) : super(dio: dio);

  @override
  Future<List<PhotoModel>> getPhotos(int albumID) async {
    final photosStringList = await performGetRequest(
      Endpoints.getPhotos(albumID),
    ) as List;
    return photosStringList.map((e) => PhotoModel.fromJson(e)).toList();
  }
}
