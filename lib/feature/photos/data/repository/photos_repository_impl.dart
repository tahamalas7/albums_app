import 'package:albumsapp/core/data/base_local_data_source.dart';
import 'package:albumsapp/core/data/base_repository.dart';
import 'package:albumsapp/core/error/exceptions.dart';
import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/core/network/network_info.dart';
import 'package:albumsapp/feature/photos/data/data_source/photos_remote_data_source.dart';
import 'package:albumsapp/feature/photos/domain/entity/photo_entity.dart';
import 'package:albumsapp/feature/photos/domain/repository/photos_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

class PhotosRepositoryImpl extends BaseRepositoryImpl
    implements PhotosRepository {
  final PhotosRemoteDataSource photosRemoteDataSource;

  PhotosRepositoryImpl({
    @required this.photosRemoteDataSource,
    @required BaseLocalDataSource baseLocalDataSource,
    @required NetworkInfo networkInfo,
  }) : super(
          networkInfo: networkInfo,
          baseLocalDataSource: baseLocalDataSource,
        );

  @override
  Future<Either<Failure, List<PhotoEntity>>> getPhotos(int albumID) async {
    return await checkNetwork<List<PhotoEntity>>(
      () async {
        try {
          final albums = await photosRemoteDataSource.getPhotos(albumID);
          return Right(albums);
        } on ServerException catch (e) {
          return Left(ServerFailure(e.errorCode));
        }
      },
    );
  }
}
