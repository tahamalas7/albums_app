import 'package:albumsapp/core/data/base_local_data_source.dart';
import 'package:albumsapp/core/data/shared_preferences_keys.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class SplashLocalDataSource extends BaseLocalDataSource {
  Future<bool> get isLoggedIn;
}

class SplashLocalDataSourceImpl extends BaseLocalDataSourceImpl
    implements SplashLocalDataSource {
  SplashLocalDataSourceImpl({@required SharedPreferences sharedPreferences})
      : super(
          sharedPreferences: sharedPreferences,
        );

  @override
  Future<bool> get isLoggedIn => Future.value(
      sharedPreferences.getBool(SharedPreferencesKeys.IS_LOGGED_IN));
}
