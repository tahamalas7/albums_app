import 'package:albumsapp/core/data/base_local_data_source.dart';
import 'package:albumsapp/core/data/base_repository.dart';
import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/core/network/network_info.dart';
import 'package:albumsapp/feature/splash/data/datasource/splash_local_data_source.dart';
import 'package:albumsapp/feature/splash/domain/repository/splah_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

class SplashRepositoryImpl extends BaseRepositoryImpl
    implements SplashRepository {
  final SplashLocalDataSource splashLocalDataSource;

  SplashRepositoryImpl({
    @required this.splashLocalDataSource,
    @required NetworkInfo networkInfo,
  }) : super(
          networkInfo: networkInfo,
          baseLocalDataSource: splashLocalDataSource,
        );

  @override
  Future<Either<Failure, bool>> isLoggedIn() async {
    final isLoggedIn = await splashLocalDataSource.isLoggedIn;
    if (isLoggedIn != null) {
      return Right(isLoggedIn);
    } else {
      return Left(CacheFailure());
    }
  }
}
