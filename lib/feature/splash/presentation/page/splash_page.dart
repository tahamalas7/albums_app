import 'package:albumsapp/core/util/generate_screen.dart';
import 'package:albumsapp/feature/splash/presentation/bloc/splash_bloc.dart';
import 'package:albumsapp/feature/splash/presentation/bloc/splash_state.dart';
import 'package:albumsapp/injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  final bloc = sl<SplashBloc>();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) => bloc.screenAppeared(),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: BlocListener(
        bloc: bloc,
        listener: (_, SplashState state) {
          if (state.isLoggedIn != null) {
            if (state.isLoggedIn) {
              Navigator.of(context).pushReplacementNamed(NameScreen.ALBUMS_PAGE);
            } else {
              Navigator.of(context)
                  .pushReplacementNamed(NameScreen.LOGIN_PAGE);
            }
          }
        },
        child: Center(
          child: Text(
            'Albums app',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 40,
            ),
          ),
        ),
      ),
    );
  }
}
