library splash_state;

import 'package:built_value/built_value.dart';

part 'splash_state.g.dart';

abstract class SplashState implements Built<SplashState, SplashStateBuilder> {
  @nullable
  bool get isLoggedIn;

  SplashState._();

  factory SplashState([updates(SplashStateBuilder b)]) = _$SplashState;

  factory SplashState.initial() {
    return SplashState((b) => b);
  }
}
