import 'dart:async';

import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/feature/splash/domain/usecase/is_logged_in.dart';
import 'package:albumsapp/feature/splash/presentation/bloc/splash_event.dart';
import 'package:albumsapp/feature/splash/presentation/bloc/splash_state.dart';
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  final IsLoggedIn isLoggedInUseCase;

  SplashBloc({
    @required IsLoggedIn isLoggedIn,
  })  : assert(isLoggedIn != null),
        isLoggedInUseCase = isLoggedIn;

  void screenAppeared() {
    dispatch(ScreenAppeared());
  }

  @override
  SplashState get initialState => SplashState.initial();

  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async* {
    print('${event.toString()}');
    if (event is ScreenAppeared) {
      yield* checkIfLoggedInOrFailure(
          (await isLoggedInUseCase(IsLoggedInParams())), currentState);
    }
  }

  Stream<SplashState> checkIfLoggedInOrFailure(
    Either<Failure, bool> check,
    SplashState state,
  ) async* {
    yield* check.fold((failure) async* {
      yield state.rebuild((b) => b..isLoggedIn = false);
    }, (_) async* {
      yield state.rebuild((b) => b..isLoggedIn = true);
    });
  }
}
