import 'package:albumsapp/core/error/failures.dart';
import 'package:albumsapp/core/usecases/usecase.dart';
import 'package:albumsapp/feature/splash/domain/repository/splah_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class IsLoggedIn extends UseCase<bool, IsLoggedInParams> {
  final SplashRepository repository;

  IsLoggedIn({@required this.repository}) : assert(repository != null);

  @override
  Future<Either<Failure, bool>> call(params) async {
    return await repository.isLoggedIn();
  }
}

class IsLoggedInParams extends Equatable {}
