import 'package:albumsapp/core/util/theme_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'core/util/custom_scroll_behavior.dart';
import 'core/util/generate_screen.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
      theme: Theme.of(context).copyWith(
        primaryColor: ThemeColors.backgroundColor,
        accentColor: ThemeColors.accentColor,
        textTheme: Theme.of(context).textTheme.apply(
              bodyColor: ThemeColors.textColor,
              displayColor: ThemeColors.textColor,
          fontFamily: "Condensed",

        ),
      ),
      debugShowCheckedModeBanner: false,
      builder: (context, child) {
        return AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: Colors.transparent,
          ),
          child: ScrollConfiguration(
            behavior: CustomScrollBehavior(),
            child: child,
          ),
        );
      },
      initialRoute: NameScreen.SPLASH_PAGE,
      onGenerateRoute: GenerateScreen.onGenerate,
    );
  }
}
